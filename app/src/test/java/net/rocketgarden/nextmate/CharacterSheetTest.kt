package net.rocketgarden.nextmate

import net.rocketgarden.nextmate.database.tables.getSheetByGuid
import net.rocketgarden.nextmate.database.tables.saveSheetObservable
import net.rocketgarden.nextmate.model.CharacterSheet
import net.rocketgarden.nextmate.model.expTable
import net.rocketgarden.nextmate.util.createDebugCharacter
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.Timeout
import org.junit.runner.RunWith
import org.robolectric.RobolectricGradleTestRunner
import org.robolectric.annotation.Config
import java.math.BigInteger
import java.util.Random
import kotlin.properties.Delegates
import kotlin.reflect.KMutableMemberProperty
import kotlin.reflect.declaredProperties
import kotlin.reflect.jvm.javaSetter
import kotlin.test.assertNotEquals

RunWith(RobolectricGradleTestRunner::class)
Config(constants = BuildConfig::class, emulateSdk = 21)
public class CharacterSheetTest {

    Rule //wierd kotlin jiggery-pokery
    public fun getTestRule(): Timeout = testRule

    private val testRule: Timeout = Timeout.seconds(10);

    private var sheet: CharacterSheet by Delegates.notNull()
    val random = Random()

    Before
    public fun setup() {
        sheet = createDebugCharacter()
    }

    Test
    public fun attributeModTest() {
        sheet.attrStr = 12
        sheet.attrDex = 14
        sheet.attrCon = 16
        sheet.attrInt = 18
        sheet.attrWis = 10
        sheet.attrCha = 12

        assertEquals(1, sheet.modStr)
        assertEquals(2, sheet.modDex)
        assertEquals(3, sheet.modCon)
        assertEquals(4, sheet.modInt)
        assertEquals(0, sheet.modWis)
        assertEquals(1, sheet.modCha)
    }

    Test
    public fun databaseReadWriteTest() {
        //Huge gross test to check all primitive fields survive database
        val properties = CharacterSheet::class.declaredProperties

        for (prop in properties) {
            //oh god it's so hideous
            if (prop is KMutableMemberProperty) {
                if (prop.get(sheet) is Int) prop.javaSetter?.invoke(sheet, (Math.random() * 100).toInt())
                else if (prop.get(sheet) is String) prop.javaSetter?.invoke(sheet, (randomString()))
            }
        }

        sheet.level = (Math.random() * 20).toInt()
        sheet.experience = (sheet.experience * Math.random() * 2.0).toInt()

        val briteDatabase = NextApplication.getBriteDatabase()
        val rowId = saveSheetObservable(briteDatabase, sheet).toBlocking().first()
        assertNotEquals(-1, rowId, "Error when inserting character")

        val newSheet = getSheetByGuid(briteDatabase, sheet.guid).toBlocking().first()

        //iterate over all properties and ensure equality
        for (prop in properties) {
            val newVal = prop.get(newSheet)
            if(newVal is Int || newVal is String || newVal is Long) {
                assertEquals("${prop.name} not equal after DB", prop.get(sheet), newVal)
            }
        }
    }

    Test
    public fun wealthTest() {
        sheet.wealth = 123456;
        assertEquals(12, sheet.moneyGold)
        assertEquals(34, sheet.moneySilver)
        assertEquals(56, sheet.moneyCopper)

        sheet.moneyGold = 80
        sheet.moneySilver = 70
        sheet.moneyCopper = 60
        assertEquals(807060, sheet.wealth)
    }

    Test
    public fun levelZeroProgressTest() {
        sheet.experience = 999999
        assertEquals(20, sheet.level)

        sheet.level = 10
        assertEquals(0f, sheet.expProgress)
    }

    Test
    public fun expRangeTest() {
        sheet.experience = 0
        assertEquals(0, sheet.expFloor)
        assertEquals(expTable[1], sheet.expCeiling)

        sheet.experience = expTable[11] - 1
        assertEquals(expTable[10], sheet.expFloor)
        assertEquals(expTable[11], sheet.expCeiling)

        sheet.experience = expTable[11]
        assertEquals(expTable[11], sheet.expFloor)
        assertEquals(expTable[12], sheet.expCeiling)
    }

    Test
    public fun testSpellSlots() {
        sheet.maxSpells = 654321
        assertEquals(4, sheet.maxSpellSlots[3])
        assertEquals(6, sheet.maxSpellSlots[5])
        assertEquals(1, sheet.maxSpellSlots[0])

        sheet.usedSpells = 87923
        assertEquals(8, sheet.usedSpellSlots[4])
        assertEquals(3, sheet.usedSpellSlots[0])
        assertEquals(9, sheet.usedSpellSlots[2])
    }


    private fun randomString(): String {
        return BigInteger(130, random).toString(32)
    }
}
