package net.rocketgarden.nextmate.fragments

import android.app.Activity
import android.util.Log
import net.rocketgarden.nextmate.BuildConfig
import net.rocketgarden.nextmate.activities.CharacterProvider
import net.rocketgarden.nextmate.model.CharacterSheet
import rx.Observable
import rx.Subscription
import kotlin.properties.Delegates

/**
 * Abstract fragment class for fragments that need a character sheet.
 * Attaches to Activity:CharacterProvider and loads a sheet
 */
public abstract class CharacterFragment: Fragment() {
    private var provider: Observable<CharacterSheet> by Delegates.notNull()
    private var subscription: Subscription by Delegates.notNull()

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        if (activity is CharacterProvider) {
            provider = activity.characterProvider
        } else {
            throw IllegalArgumentException("${activity.toString()} must implement CharacterProvider")
        }
    }

    override fun onStart() {
        super.onStart()
        subscription = provider.subscribe(
                { sheet ->
                    if(BuildConfig.DEBUG) makeToast("Loaded ${sheet.name}")
                    onSheetAvailable(sheet)
                },
                { thr ->
                    makeToast("Could not load character!")
                    Log.w("NAT20", thr)
                })
    }

    override fun onStop() {
        super.onStop()
        subscription.unsubscribe()
    }

    abstract fun onSheetAvailable(sheet: CharacterSheet)
}