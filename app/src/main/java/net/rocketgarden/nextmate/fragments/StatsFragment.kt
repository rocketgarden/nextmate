package net.rocketgarden.nextmate.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.character_stats.*
import net.rocketgarden.nextmate.R
import net.rocketgarden.nextmate.model.CharacterSheet
import net.rocketgarden.nextmate.util.modFormat

public class StatsFragment : CharacterFragment(){
    private var bubbleAnimated: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.character_stats, container, false)
    }

    override fun onSheetAvailable(sheet: CharacterSheet) {
        getActivity().setTitle(sheet.name)

        char_max_hp.setText("${sheet.maxHealth}")
        char_hd.setText("${sheet.maxHitDice}")
        char_ac.setText("${sheet.armorClass}")
        char_speed.setText("${sheet.speed}")
        char_init.setText("${sheet.initiative}")

        char_proficiency.setText("${modFormat.format(sheet.proficiency)}")

        char_attr_str.setText("${sheet.attrStr}")
        char_attr_dex.setText("${sheet.attrDex}")
        char_attr_con.setText("${sheet.attrCon}")
        char_attr_int.setText("${sheet.attrInt}")
        char_attr_wis.setText("${sheet.attrWis}")
        char_attr_cha.setText("${sheet.attrCha}")

        char_mod_str.setText("${modFormat.format(sheet.modStr)}")
        char_mod_dex.setText("${modFormat.format(sheet.modDex)}")
        char_mod_con.setText("${modFormat.format(sheet.modCon)}")
        char_mod_int.setText("${modFormat.format(sheet.modInt)}")
        char_mod_wis.setText("${modFormat.format(sheet.modWis)}")
        char_mod_cha.setText("${modFormat.format(sheet.modCha)}")

    }
}
