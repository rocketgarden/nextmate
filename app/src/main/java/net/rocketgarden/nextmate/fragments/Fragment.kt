package net.rocketgarden.nextmate.fragments

import android.support.v4.app.DialogFragment
import android.widget.Toast

public open class Fragment : android.support.v4.app.Fragment() {

    private val FRAG_TAG_DIALOG = "dialog"

    protected fun showDialogFragment(dialogFragment: DialogFragment) {
        val fm = getChildFragmentManager() //use setTargetFragment to get results back
        if(fm != null){
            dialogFragment.show(fm, FRAG_TAG_DIALOG)
        }
    }

    protected fun makeToast(message: String){
        val activity = getActivity()
        if (activity != null)
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }
}
