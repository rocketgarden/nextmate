package net.rocketgarden.nextmate.fragments

import android.animation.ObjectAnimator
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.fragment_overview.*
import net.rocketgarden.nextmate.R
import net.rocketgarden.nextmate.model.CharacterSheet


public class OverviewFragment : CharacterFragment() {

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_CHARACTER_GUID = "CHARACTER_GUID"

        // TODO: Rename and change types and number of parameters
        public fun newInstance(characterGuid: String): OverviewFragment {
            val fragment = OverviewFragment()
            val args = Bundle()
            args.putString(ARG_CHARACTER_GUID, characterGuid)
            fragment.setArguments(args)
            return fragment
        }
    }

    private var bubbleAnimated: Boolean = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_overview, container, false)
    }

    override fun onStart() {
        super.onStart()
        exp_bubble.setProgressColor(getResources().getColor(R.color.accent_dk))
        exp_bubble.setRingColor(getResources().getColor(R.color.tertiary))
    }

    override fun onSheetAvailable(sheet: CharacterSheet) {
        getActivity().setTitle(sheet.name)
        health.setText("${sheet.health}/${sheet.maxHealth}")
        hitdice.setText("${sheet.hitDice}/${sheet.maxHitDice}")

        count_copper.setText(sheet.moneyCopper.toString())
        count_silver.setText(sheet.moneySilver.toString())
        count_gold.setText(sheet.moneyGold.toString())

        level.setText(sheet.level.toString())
        exp_prev.setText(sheet.expFloor.toString())
        exp_current.setText(sheet.experience.toString())
        exp_next.setText(sheet.expCeiling.toString())

        Handler().postDelayed({
            if (!bubbleAnimated)
                ObjectAnimator.ofFloat(exp_bubble, "progress", 0f, sheet.expProgress).setDuration(1200).start();
            bubbleAnimated = true;
        }, 400);
    }

}