package net.rocketgarden.nextmate.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

import org.jetbrains.annotations.NotNull;

public class RadialProgressView extends View {

    private static final float FULL_SWEEP_ANGLE = 360;
    private static final float START_ANGLE = 90;

    private final Paint paint = new Paint();
    private final RectF rectF = new RectF();

    private float progress = 0;
    private int ringColor = 0xffbdbdbd;
    private int progressColor = 0xFFFFFFFF;
    private int strokeWidthDp = 5;
    private float strokeWidthPx;


    public RadialProgressView(Context context) {
        super(context);
        init();
    }

    public RadialProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RadialProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {

        strokeWidthPx = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, strokeWidthDp, getResources().getDisplayMetrics());

        paint.setStrokeWidth(strokeWidthPx);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeCap(Paint.Cap.ROUND);

        paint.setAntiAlias(true);
    }

    @Override
    protected void onDraw(@NotNull Canvas canvas) {
        super.onDraw(canvas);

        rectF.bottom = getWidth();
        rectF.right = getHeight();
        rectF.top = 0;
        rectF.left = 0;
        rectF.inset(strokeWidthPx / 2, strokeWidthPx / 2);

        float sweepAngle = progress * FULL_SWEEP_ANGLE;

        paint.setColor(ringColor);
        canvas.drawArc(rectF, START_ANGLE, FULL_SWEEP_ANGLE, false, paint);

        paint.setColor(progressColor);
        canvas.drawArc(rectF, START_ANGLE, sweepAngle, false, paint);
    }

    public void setProgress(float progress) {
        this.progress = progress;
        invalidate();
    }

    public void setRingColor(int ringColor) {
        this.ringColor = ringColor;
        invalidate();
    }

    public void setProgressColor(int progressColor) {
        this.progressColor = progressColor;
        invalidate();
    }

    public void setStrokeWidthDp(int strokeWidthDp) {
        this.strokeWidthDp = strokeWidthDp;
        invalidate();
    }
}

