package net.rocketgarden.nextmate.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

public open class Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
}
