package net.rocketgarden.nextmate.activities

import android.content.res.Configuration
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.view.MenuItem
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.activity_main.drawer_layout
import kotlinx.android.synthetic.activity_main.navigation
import kotlinx.android.synthetic.activity_main.toolbar
import kotlinx.android.synthetic.drawer_header.drawer_hero
import net.rocketgarden.nextmate.NextApplication
import net.rocketgarden.nextmate.R
import net.rocketgarden.nextmate.database.tables.getAllSheets
import net.rocketgarden.nextmate.fragments.OverviewFragment
import net.rocketgarden.nextmate.fragments.StatsFragment
import net.rocketgarden.nextmate.model.CharacterSheet
import net.rocketgarden.nextmate.util.subserveOn
import rx.Observable
import rx.subjects.BehaviorSubject
import rx.subscriptions.CompositeSubscription
import kotlin.properties.Delegates

public class MainActivity : Activity(), NavigationView.OnNavigationItemSelectedListener, CharacterProvider {

    private val DRAWER_CLOSE_DELAY_MS: Long = 250
    private val NAV_ITEM_ID: String = "navItemId"

    private val drawerActionHandler: Handler = Handler()

    private var drawerToggle: ActionBarDrawerToggle by Delegates.notNull()
    private var navItemId: Int = R.id.drawer_overview

    override  val characterProvider: BehaviorSubject<CharacterSheet> = BehaviorSubject.create()

    private var subscriptions: CompositeSubscription = CompositeSubscription()

    override fun onCreate(savedInstanceState: Bundle?) {
        super<Activity>.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        Picasso.with(this).load(R.raw.castle_hero).fit().centerCrop().into(drawer_hero)

        navItemId = savedInstanceState?.getInt(NAV_ITEM_ID) ?: R.id.drawer_overview

        navigation.setNavigationItemSelectedListener(this)

        drawerToggle = ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.app_name, R.string.app_name)
        drawer_layout.setDrawerListener(drawerToggle)
        drawerToggle.syncState()

        navigate(navItemId)

        val briteDatabase = NextApplication.getBriteDatabase()
        var observable = getAllSheets(briteDatabase).map { list -> list.get(0) }

        subscriptions.add(observable.subserveOn().subscribe(characterProvider))
    }

    override fun onDestroy() {
        super<Activity>.onDestroy()
        subscriptions.clear()
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        menuItem.setChecked(true)
        navItemId = menuItem.getItemId()

        drawer_layout.closeDrawer(GravityCompat.START)
        drawerActionHandler.postDelayed({ -> navigate(menuItem.getItemId()) }, DRAWER_CLOSE_DELAY_MS)

        return true
    }

    private fun navigate(itemId: Int) {

        var fragment = when(itemId){
            R.id.drawer_overview ->  OverviewFragment.newInstance("")
            R.id.drawer_sheet -> StatsFragment()
            else -> {
                OverviewFragment()
            }
        }

        val tx = getSupportFragmentManager().beginTransaction()
        tx.replace(R.id.content, fragment)
        tx.commit()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super<Activity>.onBackPressed()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.getItemId() == android.support.v7.appcompat.R.id.home) {
            drawerToggle.onOptionsItemSelected(item)
        }
        return super<Activity>.onOptionsItemSelected(item)
    }

    //navdrawer override boilerplate

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super<Activity>.onConfigurationChanged(newConfig)
        drawerToggle.onConfigurationChanged(newConfig)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super<Activity>.onSaveInstanceState(outState)
        outState?.putInt(NAV_ITEM_ID, navItemId)
    }
}

interface CharacterProvider {
    val characterProvider: Observable<CharacterSheet>
}
