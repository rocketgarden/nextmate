package net.rocketgarden.nextmate.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import net.rocketgarden.nextmate.NextApplication

import net.rocketgarden.nextmate.R
import net.rocketgarden.nextmate.database.tables.getSheetByGuid
import net.rocketgarden.nextmate.model.CharacterSheet
import net.rocketgarden.nextmate.util.subserveOn

public class EditCharacterActivity : Activity() {

    companion object {

        public val EXTRA_GUID: String = "EditCharacterActivity.EXTRA_GUID"

        public fun createIntent(ctx: Context, guid: String): Intent {
            val intent = Intent(ctx, javaClass<EditCharacterActivity>())
            intent.putExtra(EXTRA_GUID, guid)

            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_csheet)
        val guid = getIntent().getStringExtra(EXTRA_GUID)

        getSheetByGuid(NextApplication.getBriteDatabase(), guid)
                .subserveOn().first().subscribe { sheet -> processCharacterSheet(sheet) }
    }

    fun processCharacterSheet(sheet: CharacterSheet){
        //todo ugh
    }


}
