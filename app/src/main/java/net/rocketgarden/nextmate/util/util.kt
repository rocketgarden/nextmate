package net.rocketgarden.nextmate.util

import net.rocketgarden.nextmate.model.CharacterSheet
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.text.DecimalFormat
import java.util.UUID

/**
 * SubscribeOn io thread and observeOn UI thread
 */
fun <T> Observable<T>.subserveOn(): Observable<T>{
    return this.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}

val modFormat: DecimalFormat = DecimalFormat("+#0;\u2212#");


fun createDebugCharacter(): CharacterSheet{
    val cs = CharacterSheet(UUID.randomUUID().toString())
    val hp: Int = (Math.random() * 50 + 30).toInt()

    cs.name = "John Adams"
    cs.job = "Fighter"
    cs.race = "American"

    cs.experience = (Math.random() * 355000).toInt()
    cs.wealth = (Math.random() * 500000).toLong() + 500000L

    cs.maxHealth = hp
    cs.health = (hp * 0.8).toInt()
    cs.maxHitDice = cs.level
    cs.hitDice = cs.maxHitDice/2

    cs.armorClass = 13
    cs.speed = 35
    cs.initiative = 2
    cs.proficiency = 3

    cs.attrStr = 19
    cs.attrDex = 8
    cs.attrCon = 12
    cs.attrInt = 15
    cs.attrWis = 18
    cs.attrCha = 20

    return cs

}