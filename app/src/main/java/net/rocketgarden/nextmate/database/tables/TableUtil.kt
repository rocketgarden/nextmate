package net.rocketgarden.nextmate.database.tables

import android.database.Cursor

/** Convenience function to read a boolean value via column name.  */
public fun Cursor.getBoolean(column: String): Boolean {
    return this.getInt(this.getColumnIndex(column)) != 0
}

/** Convenience function to read a double value via column name.  */
public fun Cursor.getDouble(column: String): Double {
    return this.getDouble(this.getColumnIndex(column))
}

/** Convenience function to read a double value via column name.  */
public fun Cursor.getFloat(column: String): Float {
    return this.getFloat(this.getColumnIndex(column))
}

/** Convenience function to read an int value via column name.  */
public fun Cursor.getInt(column: String): Int {
    val columnIndex = this.getColumnIndex(column)
    return if (this.isNull(columnIndex)) 0 else this.getInt(columnIndex)
}

/** Convenience function to read a long value via column name.  */
public fun Cursor.getLong(column: String): Long {
    return this.getLong(this.getColumnIndex(column))
}

/** Convenience function to read a String value via column name.  */
public fun Cursor.getString(column: String): String {
    return this.getString(this.getColumnIndex(column))
}