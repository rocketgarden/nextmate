package net.rocketgarden.nextmate.database.tables

import android.content.ContentValues
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import com.squareup.sqlbrite.BriteDatabase
import com.squareup.sqlbrite.SqlBrite
import net.rocketgarden.nextmate.model.CharacterSheet
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.util.ArrayList

public val CharacterTableName: String = "CharacterSheets"

public fun createCharacterTable(db: SQLiteDatabase) {
    db.execSQL(SqlCreateStatement)
}

public fun getAllSheets(brite: BriteDatabase): Observable<List<CharacterSheet>> {

    return brite.createQuery(CharacterTableName, "SELECT * FROM " + CharacterTableName)
            .map(( fun(query: SqlBrite.Query): List<CharacterSheet> { //map cursor into list of sheets
                val cursor = query.run()
                val list = ArrayList<CharacterSheet>(cursor.getCount())

                cursor.moveToFirst()
                while (!cursor.isAfterLast()){
                    list.add(cursorToSheet(cursor))
                    cursor.moveToNext()
                }
                return list
            }))
}

public fun getSheetByGuid(brite: BriteDatabase, guid: String): Observable<CharacterSheet>{
    return brite.createQuery(CharacterTableName, "SELECT * FROM $CharacterTableName WHERE $colGuid = ?", guid)
            .map(( fun(query: SqlBrite.Query): CharacterSheet { //map cursor into sheet
                val cursor = query.run()
                cursor.moveToFirst()
                return cursorToSheet(cursor)
            }))
}

public fun saveSheet(brite: BriteDatabase, sheet: CharacterSheet){
    saveSheetObservable(brite, sheet).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe()
}

public fun saveSheetObservable(brite: BriteDatabase, sheet: CharacterSheet): Observable<Long> {
    return Observable.defer({
        Observable.just(brite.insert(CharacterTableName, sheet.toContentValues(), SQLiteDatabase.CONFLICT_REPLACE))
    })
}

public fun cursorToSheet(cursor: Cursor): CharacterSheet {
    val cs = CharacterSheet((cursor.getString(colGuid)))
    cs.name             = cursor.getString(colName)
    cs.job              = cursor.getString(colClass)
    cs.background       = cursor.getString(colBackground)
    cs.race             = cursor.getString(colRace)
    cs.alignment        = cursor.getString(colAlignment)
    cs.experience       = cursor.getInt(colExperience)
    cs.wealth           = cursor.getLong(colMoney)
    cs.health           = cursor.getInt(colHealth)
    cs.maxHealth        = cursor.getInt(colMaxHealth)
    cs.tempHealth       = cursor.getInt(colTempHealth)
    cs.hitDice          = cursor.getInt(colHitDice)
    cs.maxHitDice       = cursor.getInt(colMaxHitDice)
    cs.armorClass       = cursor.getInt(colArmorClass)
    cs.speed            = cursor.getInt(colSpeed)
    cs.initiative       = cursor.getInt(colInitiative)
    cs.proficiency      = cursor.getInt(colProficiency)
    cs.attrStr          = cursor.getInt(colStr)
    cs.attrDex          = cursor.getInt(colDex)
    cs.attrCon          = cursor.getInt(colCon)
    cs.attrInt          = cursor.getInt(colInt)
    cs.attrWis          = cursor.getInt(colWis)
    cs.attrCha          = cursor.getInt(colCha)
    cs.maxSpells = cursor.getInt(colMaxSpellSlots)
    cs.usedSpells = cursor.getInt(colUsedSpellSlots)
    cs.deathFails       = cursor.getInt(colDeathFails)
    cs.deathSuccesses   = cursor.getInt(colDeathSuccesses)
    cs.conditionFlags   = cursor.getInt(colConditionFlags)
    cs.exhaustion       = cursor.getInt(colExhaustion)

    return cs;
}

public fun CharacterSheet.toContentValues(): ContentValues {
    val cv = ContentValues(28)
    
    cv.put(colGuid, this.guid)
    cv.put(colName, this.name)
    cv.put(colClass, this.job)
    cv.put(colBackground, this.background)
    cv.put(colRace, this.race)
    cv.put(colAlignment, this.alignment)
    cv.put(colExperience, this.experience)
    cv.put(colMoney, this.wealth)
    cv.put(colHealth, this.health)
    cv.put(colMaxHealth, this.maxHealth)
    cv.put(colTempHealth, this.tempHealth)
    cv.put(colHitDice, this.hitDice)
    cv.put(colMaxHitDice, this.maxHitDice)
    cv.put(colArmorClass, this.armorClass)
    cv.put(colSpeed, this.speed)
    cv.put(colInitiative, this.initiative)
    cv.put(colProficiency, this.proficiency)
    cv.put(colStr, this.attrStr)
    cv.put(colDex, this.attrDex)
    cv.put(colCon, this.attrCon)
    cv.put(colInt, this.attrInt)
    cv.put(colWis, this.attrWis)
    cv.put(colCha, this.attrCha)
    cv.put(colMaxSpellSlots, this.maxSpells)
    cv.put(colUsedSpellSlots, this.usedSpells)
    cv.put(colDeathFails, this.deathFails)
    cv.put(colDeathSuccesses, this.deathSuccesses)
    cv.put(colConditionFlags, this.conditionFlags)
    cv.put(colExhaustion, this.exhaustion)
    
    return cv
}

private val colGuid: String = "Guid"
private val colName: String = "Name"
private val colClass: String = "Class"
private val colBackground: String = "Background"
private val colRace: String = "Race"
private val colAlignment: String = "Alignment"
private val colExperience: String = "Experience"
private val colMoney: String = "Money"
private val colHealth: String = "Health"
private val colMaxHealth: String = "MaxHealth"
private val colTempHealth: String = "TempHealth"
private val colHitDice: String = "HitDice"
private val colMaxHitDice: String = "MaxHitDice"
private val colArmorClass: String = "ArmorClass"
private val colSpeed: String = "Speed"
private val colInitiative: String = "Initiative"
private val colProficiency: String = "Proficiency"
private val colStr: String = "Str"
private val colDex: String = "Dex"
private val colCon: String = "Con"
private val colInt: String = "Int"
private val colWis: String = "Wis"
private val colCha: String = "Cha"
private val colMaxSpellSlots: String = "MaxSpellSlots"
private val colUsedSpellSlots: String = "UsedSpellSlots"
private val colDeathFails: String = "DeathFails"
private val colDeathSuccesses: String = "DeathSuccesses"
private val colConditionFlags: String = "ConditionFlags"
private val colExhaustion: String = "Exhaustion"

private val SqlCreateStatement: String =
        "CREATE TABLE IF NOT EXISTS " + CharacterTableName +
                "(" +
                colGuid             + " TEXT PRIMARY KEY," +
                colName             + " TEXT," +
                colClass            + " TEXT," +
                colBackground       + " TEXT," +
                colRace             + " TEXT," +
                colAlignment        + " TEXT," +
                colExperience       + " INTEGER," +
                colMoney            + " INTEGER," +
                colHealth           + " INTEGER," +
                colMaxHealth        + " INTEGER," +
                colTempHealth       + " INTEGER," +
                colHitDice          + " INTEGER," +
                colMaxHitDice       + " INTEGER," +
                colArmorClass       + " INTEGER," +
                colSpeed            + " INTEGER," +
                colInitiative       + " INTEGER," +
                colProficiency      + " INTEGER," +
                colStr              + " INTEGER," +
                colDex              + " INTEGER," +
                colCon              + " INTEGER," +
                colInt              + " INTEGER," +
                colWis              + " INTEGER," +
                colCha              + " INTEGER," +
                colMaxSpellSlots    + " INTEGER," +
                colUsedSpellSlots   + " INTEGER," +
                colDeathFails       + " INTEGER," +
                colDeathSuccesses   + " INTEGER," +
                colConditionFlags   + " INTEGER," +
                colExhaustion       + " INTEGER)" ;



