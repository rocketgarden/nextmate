package net.rocketgarden.nextmate.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import net.rocketgarden.nextmate.database.tables.CharacterTableName
import net.rocketgarden.nextmate.database.tables.createCharacterTable
import net.rocketgarden.nextmate.database.tables.toContentValues
import net.rocketgarden.nextmate.util.createDebugCharacter

private val DATABASE_NAME = "nextmate.db"
private val DATABASE_VERSION = 1

public class DatabaseHelper(context: Context) :
        SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    override fun onConfigure(db: SQLiteDatabase) {
        super.onConfigure(db)
        if (db.isReadOnly()) {
            // Enable foreign key constraints
            db.setForeignKeyConstraintsEnabled(true) //add integrity check??
        }
    }

    override fun onCreate(sqLiteDatabase: SQLiteDatabase) {
        createCharacterTable(sqLiteDatabase)
        val cv = createDebugCharacter().toContentValues()
        sqLiteDatabase.insertWithOnConflict(CharacterTableName, null, cv, SQLiteDatabase.CONFLICT_REPLACE)
    }

    override fun onUpgrade(sqLiteDatabase: SQLiteDatabase, i: Int, i1: Int) {

    }
}
