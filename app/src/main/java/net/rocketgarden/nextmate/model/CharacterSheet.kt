package net.rocketgarden.nextmate.model

val expTable: IntArray = intArrayOf(//level i <-> [i-1] < xp < [i]
        0, 300, 900, 2700, 6500, 14000, 23000, 34000, 48000, 64000, 85000,
        100000, 120000, 140000, 165000, 195000, 225000, 265000, 305000, 355000)

public class CharacterSheet(val guid: String) {

    var name: String = ""
    var job: String = ""
    var background: String = ""
    var race: String = ""
    var alignment: String = ""

    var experience = 0
    var level: Int
        get() {
            for (i in expTable.indices) {
                if (experience < expTable[i])
                    return i
            }
            return expTable.size()
        }
        set(value) {
            experience = expTable[Math.max(Math.min(value, expTable.size())-1, 0)]
        }
    val expFloor: Int
        get() {
            return expTable[level - 1]
        }

    val expCeiling: Int
        get() {
            return expTable[Math.min(expTable.size(), level)]
        }

    val expProgress: Float
        get() {
            return (experience - expFloor) / (expCeiling - expFloor).toFloat()
        }

    var moneyGold: Int = 0
    var moneySilver: Int = 0
    var moneyCopper: Int = 0

    var wealth: Long
        get() {
            var total: Long = 0
            total += moneyCopper
            total += moneySilver * 100L
            total += moneyGold.toLong() * 100L * 100L
            return total
        }
        set(value) {
            val copper = value.toInt()
            moneyCopper = copper % 100
            moneySilver = (copper / 100) % 100
            moneyGold = copper / (100 * 100)
        }

    var health: Int = 0
    var maxHealth: Int = 0
    var tempHealth: Int = 0
    var hitDice: Int = 0
    var maxHitDice: Int = 0

    var armorClass: Int = 0
    var speed: Int = 0
    var initiative: Int = 0
    var proficiency: Int = 0

    var attrStr: Int = 0
    var attrDex: Int = 0
    var attrCon: Int = 0
    var attrInt: Int = 0
    var attrWis: Int = 0
    var attrCha: Int = 0

    val modStr: Int get() = attrToMod(attrStr)
    val modDex: Int get() = attrToMod(attrDex)
    val modCon: Int get() = attrToMod(attrCon)
    val modInt: Int get() = attrToMod(attrInt)
    val modWis: Int get() = attrToMod(attrWis)
    val modCha: Int get() = attrToMod(attrCha)

    //    spell slots as digits of integer. For serialization
    var maxSpells: Int = 0
    var usedSpells: Int = 0

    val maxSpellSlots: Array<Int>
        get() {
            return Array(10, {
                i -> (maxSpells/(Math.pow(10.0, i.toDouble()).toInt()))%10
            })
        }

    val usedSpellSlots: Array<Int>
        get() {
            return Array(10, {
                i -> (usedSpells/(Math.pow(10.0, i.toDouble()).toInt()))%10
            })
        }

    var deathFails: Int = 0
    var deathSuccesses: Int = 0
    var conditionFlags: Int = 0
    var exhaustion: Int = 0

    fun attrToMod(attr: Int): Int {
        return attr / 2 - 5
    }
}
