package net.rocketgarden.nextmate;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;

import net.rocketgarden.nextmate.database.DatabaseHelper;

import io.fabric.sdk.android.Fabric;

public class NextApplication extends Application {

    private static DatabaseHelper dbHelper;
    private static BriteDatabase briteDb;

    public static BriteDatabase getBriteDatabase(){
        return briteDb;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        setupCrashlytics();

        dbHelper = new DatabaseHelper(this);
        briteDb = SqlBrite.create().wrapDatabaseHelper(dbHelper);
    }

    protected void setupCrashlytics() { //override in test
        Fabric.with(this, new Crashlytics());
    }
}
